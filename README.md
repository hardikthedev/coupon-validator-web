# Coupon code validator web

This is a React project build on top of CRA templat
Once the server is up you are ready to get web app runnning locally 

# To run this project locally 
- Clone this repo
- Create a .env file in the root directory 
- Add server URL  (remote or local) in `REACT_APP_SERVER_URL` variable in env file
- Run `npm run start` to start the react development server

This app is deployed on netlify using their automatic CI/CD support for Gitlab.
URL  : https://upbeat-kepler-1ccd9a.netlify.app/
