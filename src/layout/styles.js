import { Content as AntContent } from "antd/lib/layout/layout"
import styled from "styled-components"

export const ContentContainer = styled(AntContent)`
  height: 100vh;
  display: flex;
  justify-content: center;
`
export const ContentDiv = styled.div`
  max-width: 1440px;
  width: 100%;
  padding: 2rem;
`