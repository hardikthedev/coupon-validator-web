import Title from "antd/lib/typography/Title"
import { HeaderContainer } from "./styles"

function Header() {
  return (
    <HeaderContainer>
      <Title level={4}>Coupon Validator</Title>
    </HeaderContainer>
  )
}

export default Header
