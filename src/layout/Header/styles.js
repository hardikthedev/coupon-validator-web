import { Header as AntHeader } from "antd/lib/layout/layout"
import styled from "styled-components"

export const HeaderContainer = styled(AntHeader)`
  display: flex;
  align-items: center;
  color: #fff;
  .ant-typography {
    color: #fff;
    margin: 0px;
  }
`