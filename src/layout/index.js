import { Layout as AntLayout } from 'antd';
import Header from './Header';
import { ContentContainer, ContentDiv } from './styles';

function Layout({ children }) {
  return (
    <AntLayout>
      <Header>Header</Header>
      <ContentContainer>
        <ContentDiv>
          {children}
        </ContentDiv>
      </ContentContainer>
    </AntLayout>
  )
}

export default Layout
