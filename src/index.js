import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import App from './App';
import { ApolloClient, InMemoryCache, ApolloProvider, from, HttpLink } from "@apollo/client";
import { ErrorLink } from '@apollo/link-error';
import { message } from 'antd';

const errorLink = new ErrorLink(({ graphQLErrors, networkError, response }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message: errorMessage, locations, path }) => {
      message.destroy();
      message.error(errorMessage, 4);
      return console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      );
    });
  }
  if (response) {
    response.errors.map(({ message: errorMessage, locations, path }) => {
      message.destroy();
      message.error(errorMessage);
      return console.log(
        `[Response error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      );
    });
  }
  if (networkError) {
    message.destroy();
    message.error('Network error');
    console.log(`[Network error]: ${networkError}`);
  }
});

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_SERVER_URL
})

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: from([errorLink, httpLink]),
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
