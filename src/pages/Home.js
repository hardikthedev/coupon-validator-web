import { useState } from "react"
import CouponList from "../components/CouponList"
import Validator from "../components/Validator"

function Home() {
  const [selectedCoupon, setSelectedCoupon] = useState("")
  return (
    <div>
      <Validator selectedCoupon={selectedCoupon} setSelectedCoupon={setSelectedCoupon} />
      <CouponList setSelectedCoupon={setSelectedCoupon} />
    </div>
  )
}

export default Home
