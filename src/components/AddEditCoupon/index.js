import { useEffect, useState } from 'react';
import Modal from 'antd/lib/modal';
import Form from 'antd/lib/form'
import Input from 'antd/lib/input';
import InputNumber from 'antd/lib/input-number';
import DatePicker from 'antd/lib/date-picker';
import Select from 'antd/lib/select';
import moment from 'moment';
import { useForm } from 'antd/lib/form/Form';
import { useMutation } from '@apollo/client';
import { ADD_COUPON, EDIT_COUPON } from '../../graphql/Mutations';
import { COUPON_LIST } from '../../graphql/Queries';
import { message } from 'antd';

const { RangePicker } = DatePicker
const { Option } = Select

function AddEditModal({ triggerButton, mode = 'ADD', editData }) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [addCoupon] = useMutation(ADD_COUPON)
  const [editCoupon] = useMutation(EDIT_COUPON)
  const [discountType, setDiscountType] = useState('FLAT')
  const [loading, setLoading] = useState(false)
  const [initialValues, setInitialValues] = useState()
  const [form] = useForm()

  useEffect(() => {
    if (editData && mode === 'EDIT') {
      const { couponCode, description, discountType, validFrom, validTo, flatValue, percentValue, maxAmount } = editData
      setDiscountType(discountType)
      const prefill = {
        couponCode,
        description,
        discountType,
        validity: [moment(validFrom), moment(validTo)],
        value: discountType === 'FLAT' ? flatValue : percentValue,
        maxAmount
      }
      setInitialValues(prefill)
    }
  }, [editData, mode])

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    form.submit();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onSubmit = async (e) => {
    try {
      setLoading(true)
      const { couponCode, description, validity, discountType, value, maxAmount } = e
      const data = {
        couponCode,
        description,
        validFrom: moment(validity[0]).toISOString(),
        validTo: moment(validity[1]).toISOString(),
        discountType,
        flatValue: discountType === 'FLAT' ? value : 0,
        percentValue: discountType === 'PERCENT' ? value : 0,
        maxAmount
      }
      const mutation = mode === 'ADD' ? addCoupon : editCoupon
      const variables = mode === 'ADD' ? {
        createCouponData: data
      } : {
        updateCouponData: data,
        updateCouponWhere: { couponCode: editData.couponCode }
      }
      await mutation({
        variables,
        refetchQueries: [{
          query: COUPON_LIST
        }]
      })
      const successMessage = `Coupon ${mode === 'EDIT' ? 'updated' : 'added'} successfully!`
      message.success(successMessage)
      setLoading(false)
      setIsModalVisible(false)
      form.resetFields()
    } catch (e) {
      setLoading(false)
      console.log(e)
    }
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => form.resetFields(), [initialValues]);

  return (
    <>
      <div onClick={showModal}>
        {triggerButton}
      </div>
      <Modal
        title="Add Coupon"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{ loading }}
      >
        <Form onFinish={onSubmit} form={form} initialValues={initialValues}>
          <Form.Item
            label="Coupon code"
            name="couponCode"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Description"
            name='description'
          >
            <Input.TextArea />
          </Form.Item>
          <Form.Item
            label="Validity"
            name='validity'
            rules={[{ required: true }]}
          >
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
            />
          </Form.Item>
          <Form.Item
            label="Type"
            name='discountType'
            rules={[{ required: true }]}
          >
            <Select onSelect={e => setDiscountType(e)}>
              <Option value="FLAT">FLAT</Option>
              <Option value="PERCENT">PERCENT</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Value"
            name='value'
            rules={[{ required: true }]}
          >
            <InputNumber />
          </Form.Item>
          {discountType === 'PERCENT' &&
            <Form.Item
              label="Max amount"
              name='maxAmount'
              rules={[{ required: true }]}
            >
              <InputNumber />
            </Form.Item>
          }
        </Form>
      </Modal>
    </>
  )
}

export default AddEditModal
