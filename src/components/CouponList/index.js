import { useQuery } from '@apollo/client';
import { Space } from 'antd';
import Button from 'antd/lib/button';
import Table from 'antd/lib/table';
import { useEffect, useState } from 'react';
import { COUPON_LIST } from '../../graphql/Queries';
import AddEditModal from '../AddEditCoupon';
import { VerticalAlignTopOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import Title from 'antd/lib/typography/Title';
import { CouponListHeaderDiv } from './styles';
import DeleteCoupon from '../DeleteCoupon';


function CouponList({ setSelectedCoupon }) {
  const { data, loading } = useQuery(COUPON_LIST)
  const [coupons, setCoupons] = useState([])


  useEffect(() => {
    if (!loading && data && data.couponList) {
      setCoupons(data.couponList)
    }
  }, [data, loading])

  const columns = [
    {
      title: 'Coupon Code',
      dataIndex: 'couponCode',
      key: 'couponCode',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Type',
      dataIndex: 'discountType',
      key: 'discountType',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <Space>
          <Button icon={<VerticalAlignTopOutlined />} onClick={() => { setSelectedCoupon(record.couponCode) }} />
          <AddEditModal triggerButton={<Button icon={<EditOutlined />} />} mode='EDIT' editData={record} />
          <DeleteCoupon couponCode={record.couponCode} />
        </Space>
      ),
    },
  ];

  return (
    <div>
      <CouponListHeaderDiv>
        <Title level={4}>Currently available coupons</Title>
        <AddEditModal mode='ADD' triggerButton={
          <Button
            type="primary"
            icon={<PlusOutlined />}
          >
            Add Coupon
          </Button>
        } />
      </CouponListHeaderDiv>
      <Table columns={columns} dataSource={coupons} loading={loading} />
    </div>
  )
}

export default CouponList
