import styled from "styled-components";

export const CouponListHeaderDiv = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 16px 0px;
  .ant-typography{
    margin: 0px;
  }
`