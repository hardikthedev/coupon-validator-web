import { Button, Input, message } from 'antd'
import { InputNumber } from 'antd'
import Form from 'antd/lib/form'
import { useForm } from 'antd/lib/form/Form'
import { useEffect, useState } from 'react'
import { client } from '../..'
import { VALIDATE_COUPON } from '../../graphql/Queries'

function Validator({ selectedCoupon, setSelectedCoupon }) {
  const [loading, setLoading] = useState(false)
  const [form] = useForm()
  const onSubmit = async (e) => {
    try {
      setLoading(true)
      const { couponCode, cartAmount } = e
      const res = await client.query({
        query: VALIDATE_COUPON,
        variables: {
          validateCouponCartAmount: cartAmount,
          validateCouponWhere: { couponCode }
        },
        fetchPolicy: 'network-only'
      })
      if (res?.data?.validateCoupon?.message) {
        const successMessage = res.data.validateCoupon.message
        message.success(successMessage)
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
      console.log(e)
    }
  }

  useEffect(() => {
    if (selectedCoupon) {
      form.resetFields()
      form.setFieldsValue({ couponCode: selectedCoupon })
      setSelectedCoupon()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCoupon])

  return (
    <Form layout="inline" onFinish={onSubmit} form={form}>
      <Form.Item label="Coupon code" name="couponCode" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item label="Cart amount" name="cartAmount" rules={[{ required: true }]}>
        <InputNumber />
      </Form.Item>
      <Button type="primary" htmlType="submit" loading={loading}>
        Apply coupon
      </Button>
    </Form>
  )
}

export default Validator
