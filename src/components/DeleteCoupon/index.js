import { useMutation } from '@apollo/client';
import { Button, message, Popconfirm } from 'antd'
import React, { useState } from 'react'
import { DELETE_COUPON } from '../../graphql/Mutations';
import { COUPON_LIST } from '../../graphql/Queries';
import { DeleteOutlined } from '@ant-design/icons';

function DeleteCoupon({ couponCode }) {
  const [deleteCoupon, { loading: deleteLoading }] = useMutation(DELETE_COUPON)
  const [visible, setVisible] = useState(false);

  const showPopconfirm = () => {
    setVisible(true);
  };

  const handleOk = (couponCode) => {
    deleteCoupon({
      variables: {
        deleteCouponWhere: {
          couponCode: couponCode
        }
      },
      refetchQueries: [{
        query: COUPON_LIST
      }]
    }).then(() => {
      message.success("Coupon deleted successfully")
      setVisible(false)
    }).catch(e => {
      console.log(e)
      setVisible(false)
    })
  };
  return (
    <Popconfirm
      visible={visible}
      title="Are you sure want to delete this coupon?"
      onConfirm={() => { handleOk(couponCode) }}
      onCancel={() => { setVisible(false) }}
      okButtonProps={{ loading: deleteLoading }}
    >
      <Button icon={<DeleteOutlined />} onClick={showPopconfirm} />
    </Popconfirm>
  )
}

export default DeleteCoupon
