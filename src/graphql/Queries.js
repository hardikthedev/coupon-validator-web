import gql from "graphql-tag";

export const COUPON_LIST = gql`
  query {
    couponList {
      couponCode
      description
      discountType
      validTo
      validFrom
      flatValue
      percentValue
      maxAmount
    }
  }
`

export const VALIDATE_COUPON = gql`
  query($validateCouponWhere: CouponWhereUniqueInput!, $validateCouponCartAmount: Int!){
    validateCoupon(where: $validateCouponWhere, cartAmount: $validateCouponCartAmount) {
      cartAmount
      discountedAmount
      message 
    }
  }
`