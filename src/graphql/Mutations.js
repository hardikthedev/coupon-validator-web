import gql from "graphql-tag";

export const ADD_COUPON = gql`
  mutation($createCouponData: CouponCreateUpdateInput!) {
    createCoupon(data: $createCouponData) {
      couponCode
      description
      discountType
      flatValue
      maxAmount
      percentValue
      validFrom
      validTo
    }
  }
`;

export const EDIT_COUPON = gql`
  mutation($updateCouponData: CouponCreateUpdateInput!, $updateCouponWhere: CouponWhereUniqueInput!) {
    updateCoupon(data: $updateCouponData, where: $updateCouponWhere) {
      couponCode
      description
      discountType
      flatValue
      maxAmount
      percentValue
      validFrom
      validTo
    }
  }
`;

export const DELETE_COUPON = gql`
  mutation($deleteCouponWhere: CouponWhereUniqueInput!) {
    deleteCoupon(where: $deleteCouponWhere) {
      message
    }
  }
`;