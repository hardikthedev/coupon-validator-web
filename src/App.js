import Home from "./pages/Home";
import Layout from './layout'

function App() {
  return (
    <Layout>
      <Home />
    </Layout>
  );
}

export default App;
